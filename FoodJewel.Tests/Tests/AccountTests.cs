﻿using FoodJewel.Tests.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Tests
{
    [TestClass]
    public class AccountTests:BaseTestServer
    {
        [ClassInitialize]
        public new static void Initialize(TestContext context)
        {
            BaseTestServer.Initialize(context);
        }

        [ClassCleanup]
        public new static void Cleanup()
        {
            BaseTestServer.Cleanup();
        }

        /// <summary>
        /// Check to see if Bearer tokens and refresh tokens were succesfully retrieved.
        /// </summary>
        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void CanGetBearerToken()
        {
            Assert.IsNotNull(AccessToken, "Good AccessToken");
            Assert.IsNotNull(RefreshToken, "Good RefreshToken"); 
            Assert.IsNotNull(AdminAccessToken, "Good AdminAccessToken");
        }

        /// <summary>
        /// Attempt to use the refresh token, and see if you get a new access token.
        /// </summary>
        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void CanGetRefreshToken()
        {
            HttpClient client = server.HttpClient;

            //Get unit test login
            var response = client.PostAsync("/Token",
                new FormUrlEncodedContent(
                new Dictionary<string, string>
                {
                    {"grant_type","refresh_token"},
                    {"client_id",ClientID},
                    {"client_secret",ClientSecret},
                    {"refresh_token",RefreshToken}
                })).Result;

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AccessToken = result.access_token;
                RefreshToken = result.refresh_token;
            }
            Assert.IsTrue(response.IsSuccessStatusCode, "Refresh Success");
        }


        /// <summary>
        /// Attempt to use the refresh token, and see if you get a new access token.
        /// </summary>
        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void CantReuseRefreshToken()
        {
            HttpClient client = server.HttpClient;

            var content = 
                new Dictionary<string, string>
                {
                    {"grant_type","refresh_token"},
                    {"client_id",ClientID},
                    {"client_secret",ClientSecret},
                    {"refresh_token",RefreshToken}
                };

            //Get unit test login
            var response = client.PostAsync("/Token", new FormUrlEncodedContent(content)).Result;
            dynamic result = null;

            if (response.IsSuccessStatusCode)
            {
                result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AccessToken = result.access_token;
                RefreshToken = result.refresh_token;
            }
            Assert.IsTrue(response.IsSuccessStatusCode, "Initial Refresh Should Succeed.");

            //Try to reuse the refresh token, this should fail.
            response = client.PostAsync("/Token", new FormUrlEncodedContent(content)).Result;

            Assert.IsFalse(response.IsSuccessStatusCode, "Second Refresh Should Fail.");
            result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual((string)result.error, "invalid_grant", "Error should be 'invalid_grant'.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void UserCanGetInfo()
        {
            HttpClient client = UserClient;
            var response = client.GetAsync("/api/account/userinfo/unittest").Result;  //Assuming 'unittest' is the current user accesstoken

            Assert.IsTrue(response.IsSuccessStatusCode, "Request should succeed.");
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual((string)result.UserName, "unittest", "User Name should be 'unittest'.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void UserCantGetOtherUserInfo()
        {
            HttpClient client = UserClient;
            var response = client.GetAsync("/api/account/userinfo/admin").Result;

            Assert.IsFalse(response.IsSuccessStatusCode, "Request should fail.");
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsNull(result.UserName, "UserName should be null.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void AdminCanGetOtherUserInfo()
        {
            HttpClient client = AdminClient;
            var response = client.GetAsync("/api/account/userinfo/unittest").Result;  //Assuming 'unittest' is the current user accesstoken

            Assert.IsTrue(response.IsSuccessStatusCode, "Request should succeed.");
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual((string)result.UserName, "unittest", "User Name should be 'unittest'.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void CanRegisterUser()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);

            //Cleanup
            //Delete user, using Admin login
            var client = AdminClient;
            var response = client.DeleteAsync("/api/account/userinfo/"+userName).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Delete login should succeed");
        }


        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void UserCanDeleteUser()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);

            var client = AdminClient;
            var response = client.DeleteAsync("/api/account/userinfo/"+userName).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Delete should succeed");
        }


        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void UserCantDeleteOtherUser()
        {
            string userName, password;

            RegisterTempUser(out userName, out password);

            var token = LoginUser(userName, password);

            //Try to delet the unittest user, using the register user login
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var response = client.DeleteAsync("/api/account/userinfo/unittest").Result;

            Assert.IsFalse(response.IsSuccessStatusCode, "User trying to delete another should fail.");

            //Clean up temp user, by deleting it
            response = client.DeleteAsync("/api/account/userinfo/" + userName).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Delete  tempuser, cleanup should succeed");
        }
        
        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void AdminCanDeleteOtherUser()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);

            //Delete user, using Admin login
            var client = AdminClient;
            var response = client.DeleteAsync("/api/account/userinfo/" + userName).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Delete  tempuser, from Admin login should succeed");
        }
        
        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void AdminCantDeleteSelf()
        {
            //Delete user, using Admin login
            var client = AdminClient;
            var response = client.DeleteAsync("/api/account/userinfo").Result;

            Assert.IsFalse(response.IsSuccessStatusCode, "Admin user can't delete themselves.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void UserCanChangePassword()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Change user password
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            var content = new
            {
                OldPassword = password,
                NewPassword = "Passw0rd",
                ConfirmPassword = "Passw0rd"
            };

            var response = client.PostAsJsonAsync("/api/account/changepassword", content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Succesfully changed password.");

            //Login with new password
            token = LoginUser(userName, "Passw0rd");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            //Clean up, delete user account
            response = client.DeleteAsync("/api/account/userinfo").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Delete temp user should succeed.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void AdminCanResetPassword()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Change user password
            var client = AdminClient;
            var content = new
             {
                 UserName = userName,
                 Password = "Passw0rd",
                 ConfirmPassword = "Passw0rd"
             };

            var response = client.PostAsJsonAsync("/api/account/resetpassword", content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Succesfully changed password.");

            //Login with new password
            token = LoginUser(userName, "Passw0rd");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            //Clean up, delete user account
            response = client.DeleteAsync("/api/account/userinfo").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Delete temp user should succeed.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void UserCanChangeInfo()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Chenge user FullName
            var client = server.HttpClient;
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            string newFullName = "Change Test";
            var content = new
            {
                UserName = userName,
                FullName = newFullName,
                Email = userName + "@heliconsoftware.com"
            };

            var response = client.PutAsJsonAsync("/api/account/userinfo", content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Succesfully modified UserInfo.");

            //Retrieve FullName and compare
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            response = client.GetAsync("/api/account/userinfo").Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Can request new UserInfo.");
            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual((string)result.FullName, newFullName, "Changed name is returned.");
            }

            //Clean up, delete user account
            response = client.DeleteAsync("/api/account/userinfo").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Clean up should succeed.");
        }

        [TestMethod]
        [TestCategory("OWIN TestServer")]
        [TestCategory("Authentication")]
        public void AdminCanChangeUserInfo()
        {
            string userName, password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Chenge user FullName
            var client = AdminClient;
            string newFullName = "Change Test";
            var content = new
            {
                UserName = userName,
                FullName = newFullName,
                Email = userName + "@heliconsoftware.com"
            };

            var response = client.PutAsJsonAsync("/api/account/userinfo", content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Succesfully modified UserInfo.");

            //Retrieve FullName and compare
            response = client.GetAsync("/api/account/userinfo/" + userName).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Can request new UserInfo.");
            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual((string)result.FullName, newFullName, "Changed name is returned.");
            }

            //Clean up, delete user account
            response = client.DeleteAsync("/api/account/userinfo/"+userName).Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Clean up should succeed.");
        }


    }
}
