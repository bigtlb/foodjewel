﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using Owin;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Tests.Tests
{
    public abstract class BaseTestServer
    {

        internal static TestServer server;
        internal static string ClientID = "FoodJewel-WinPhone8.1";
        internal static string ClientSecret = "ClientSecretPassword";
        internal static string AccessToken = null;
        internal static string AdminAccessToken = null;
        internal static string RefreshToken = null;

        #region Utility Methods
        public static HttpClient UserClient
        {
            get
            {
                var client = server.HttpClient;
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AccessToken);
                return client;
            }
        }

        public static HttpClient AdminClient
        {
            get
            {
                var client = server.HttpClient;
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AdminAccessToken);
                return client;
            }
        }

        protected static void RegisterTempUser(out string userName, out string password)
        {
            var client = server.HttpClient;
            var guid = Guid.NewGuid().ToString("N");
            userName = string.Format("unittestregister{0}", guid);
            password = "P@ssw0rd";// "Xx1" + guid;

            var content = new
            {
                UserName = userName,
                FullName = "unittest register name",
                Email = string.Format("unittestregister{0}@heliconsoftware.com", guid),
                Password = password,              //To fulfill password validation rules
                ConfirmPassword = password
            };
            var response = client.PostAsJsonAsync("/api/account/register", content).Result;

            Assert.IsTrue(response.IsSuccessStatusCode, "Request should succeed.");
        }

        protected static string LoginUser(string userName, string password)
        {
            var client = server.HttpClient;

            //Get unit test register login
            var response = client.PostAsync("/Token",
                new FormUrlEncodedContent(
                new Dictionary<string, string>
                {
                    {"grant_type","password"},
                    {"client_id",ClientID},
                    {"client_secret",ClientSecret},
                    {"username",userName},
                    {"password",password}
                })).Result;

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                return (string)result.access_token;
            }
            return null;
        }

        protected static void DeleteUser(string userName){
            var client = AdminClient;
            var response = client.DeleteAsync("/api/account/userinfo/"+userName).Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Succesfully deleted user {0}.", userName);
        }

        public static void Initialize(TestContext context)
        {
            server = TestServer.Create(app =>
                {
                    var startup = new Startup();
                    var config = new HttpConfiguration();
                    
                    WebApiConfig.Register(config);
                    UnityConfig.RegisterComponents(config);

                    startup.ConfigureAuth(app);
                    app.UseWebApi(config);
                });

            //Initialize login

            var client = server.HttpClient;

            //Get unit test login
            var response = client.PostAsync("/Token",
                new FormUrlEncodedContent(
                new Dictionary<string, string>
                {
                    {"grant_type","password"},
                    {"client_id",ClientID},
                    {"client_secret",ClientSecret},
                    {"username","unittest"},
                    {"password","Unittest1"}
                })).Result;

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AccessToken = result.access_token;
                RefreshToken = result.refresh_token;
            }

            //Get Admin login
            response = client.PostAsync("/Token",
                new FormUrlEncodedContent(
                new Dictionary<string, string>
                {
                    {"grant_type","password"},
                    {"client_id",ClientID},
                    {"client_secret",ClientSecret},
                    {"username","admin"},
                    {"password","P@ssw0rd"}
                })).Result;

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                AdminAccessToken = result.access_token;
            }
        }

        public static void Cleanup()
        {
            server.Dispose();
        }        
        #endregion
    }
}
