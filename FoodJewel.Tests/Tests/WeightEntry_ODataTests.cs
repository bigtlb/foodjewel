﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace FoodJewel.Tests.Tests
{
    [TestClass]
    public class WeightEntry_ODataTests: BaseTestServer
    {

        [ClassInitialize]
        public new static void Initialize(TestContext context)
        {
            BaseTestServer.Initialize(context);
        }

        [ClassCleanup]
        public new static void Cleanup()
        {
            BaseTestServer.Cleanup();
        }

        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_UserCantGetOthers()
        {
            //Arrange
            var client = UserClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Get the Unit Test JournalUser info
            var response = client.GetAsync("odata/JournalUsers").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Should be able to get info on current JournalUser.");
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int userId = result.value[0].Id;

            //Make weight entry
            var key = MakeWeightEntry(client, null, DateTime.UtcNow, 100M, "kg");

            //Make a new temp user
            string userName;
            string password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Get Temp User JournalUser info
            var tempclient = server.HttpClient;
            tempclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            tempclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            response = tempclient.GetAsync("odata/JournalUsers").Result;
            result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int tempUserId = result.value[0].Id;

            //Make Weight entry
            var tempkey = MakeWeightEntry(tempclient, null, DateTime.UtcNow, 101M, "kg");

            //Assert
            response = client.GetAsync(string.Format("odata/WeightEntries?$filter=UserId+eq+{0}", userId)).Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Should be able to get entries using your userid.");
            result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsTrue(result.value.Count > 0, "Should be at least one entry in results.");
            var values = result.value as JArray;
            Assert.IsFalse(values.Any(x => (int)x["Id"] == tempkey), "Should not contain entries from other user.");
            Assert.IsTrue(values.Any(x => (int)x["Id"] == key), "Must contain entry from this user.");

            response = client.GetAsync(string.Format("odata/WeightEntries?$filter=UserId+eq+{0}", tempUserId)).Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "OData query will not fail.");
            result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.IsTrue(result.value.Count == 0, "Results for another user must be empty.");

            //Cleanup
            DeleteWeightEntry(client, key);
            DeleteWeightEntry(tempclient, tempkey);
            DeleteUser(userName);
        }


        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_UserCantGetOtherSpecificEntry()
        {
            //Arrange
            var client = UserClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Make a new temp user
            string userName;
            string password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Get Temp User JournalUser info
            var tempclient = server.HttpClient;
            tempclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            tempclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = tempclient.GetAsync("odata/JournalUsers").Result;
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int tempUserId = result.value[0].Id;

            //Make Weight entry
            var tempkey = MakeWeightEntry(tempclient, null, DateTime.UtcNow, 101M, "kg");

            //Assert
            response = client.GetAsync(string.Format("odata/WeightEntries({0})", tempkey)).Result;
            Assert.IsFalse(response.IsSuccessStatusCode, "Should not be able to find another users entries, even if we have a key.");

            //Cleanup
            DeleteWeightEntry(tempclient, tempkey);
            DeleteUser(userName);
        }

        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_UserCanPostSelf()
        {
            //Arrange
            var client = UserClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var key = MakeWeightEntry(client, null, DateTime.UtcNow, 200M, "lb");

            //Assert
            var response = client.GetAsync("odata/WeightEntries").Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Get for weight entries should succeed.");
            if (response.IsSuccessStatusCode)
            {
                var result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                var values = result["value"];
                Assert.IsTrue(values.Any(_ => (int)_["Id"] == key), "Weight entry should be in the retrieved list.");
            }

            //Cleanup
            DeleteWeightEntry(client, key);
        }


        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_UserCantPostForOthers()
        {
            //Arrange
            var client = UserClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Make a new temp user
            string userName;
            string password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Get Temp User JournalUser info
            var tempclient = server.HttpClient;
            tempclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            tempclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = tempclient.GetAsync("odata/JournalUsers").Result;
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int tempUserId = result.value[0].Id;

            //Assert
            //This entry should fail
            MakeWeightEntry(client, tempUserId, DateTime.UtcNow, 100M, "kg", false);

            //Cleanup
            DeleteUser(userName);
        }


        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_AdminCanPostForOthers()
        {
            //Arrange
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Make a new temp user
            string userName;
            string password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Get Temp User JournalUser info
            var tempclient = server.HttpClient;
            tempclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            tempclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = tempclient.GetAsync("odata/JournalUsers").Result;
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int tempUserId = result.value[0].Id;

            //Assert
            //This entry should fail
            var tempkey = MakeWeightEntry(client, tempUserId, DateTime.UtcNow, 100M, "kg");

            //Cleanup
            DeleteWeightEntry(client, tempkey);
            DeleteUser(userName);
        }


        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_AdminCanGetOthers()
        {
            //Arrange
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Make a new temp user
            string userName;
            string password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Get Temp User JournalUser info
            var tempclient = server.HttpClient;
            tempclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            tempclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = tempclient.GetAsync("odata/JournalUsers").Result;
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int tempUserId = result.value[0].Id;
            var tempkey = MakeWeightEntry(tempclient, null, DateTime.UtcNow, 100M, "kg");

            //Assert
            response = client.GetAsync(string.Format("odata/WeightEntries?$filter=UserId+eq+{0}", tempUserId)).Result;
            result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            var values = result.value as JArray;
            Assert.IsTrue(values.Any(x => (int)x["Id"] == tempkey), "Other users weight entry should be in the list");

            //Cleanup
            DeleteWeightEntry(client, tempkey);
            DeleteUser(userName);
        }


        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_AdminCanGetOtherSpecificEntry()
        {
            //Arrange
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Make a new temp user
            string userName;
            string password;
            RegisterTempUser(out userName, out password);
            var token = LoginUser(userName, password);

            //Get Temp User JournalUser info
            var tempclient = server.HttpClient;
            tempclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            tempclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = tempclient.GetAsync("odata/JournalUsers").Result;
            dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            int tempUserId = result.value[0].Id;

            //Make Weight entry
            var tempkey = MakeWeightEntry(tempclient, null, DateTime.UtcNow, 101M, "kg");

            //Assert
            response = client.GetAsync(string.Format("odata/WeightEntries({0})", tempkey)).Result;
            Assert.IsTrue(response.IsSuccessStatusCode, "Administrator must be able to find another users entries.");

            //Cleanup
            DeleteWeightEntry(tempclient, tempkey);
            DeleteUser(userName);
        }

        [TestMethod]
        [TestCategory("WeightEntry")]
        [TestCategory("OData")]
        [TestCategory("OWIN TestServer")]
        public void WeightEntry_UserCanPutSelf()
        {
            //Arrange
            var client = AdminClient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private void DeleteWeightEntry(HttpClient client, int key, bool expectSuccess = true)
        {
            var response = client.DeleteAsync(string.Format("odata/WeightEntries({0})", key)).Result;

            // Response should match the expectSuccess flag
            Assert.IsTrue(response.IsSuccessStatusCode == expectSuccess, "Weight entry delete for {0} should {1}.", key, expectSuccess ? "succeed" : "fail");

        }

        private int MakeWeightEntry(HttpClient client, int? userId, DateTime date, decimal weight, string units, bool expectSuccess = true)
        {

            //Try posting yourself
            var weightEntry = new
            {
                UserId = userId??0,
                Date = date,
                Weight = new { Value = weight, Format = units }
            };

            var response = client.PostAsJsonAsync("/odata/WeightEntries", weightEntry).Result;

            // Response should match the expectSuccess flag
            Assert.IsTrue(response.IsSuccessStatusCode == expectSuccess, "Weight entry post should {0}.", expectSuccess?"succeed":"fail");

            if (response.IsSuccessStatusCode)
            {
                dynamic result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                return result.Id;
            }
            else
            return 0;
        }
    }
}
