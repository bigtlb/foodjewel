﻿using System.Web;
using System.Web.Optimization;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/bower_components/bootstrap/dist/css/bootstrap.css",
                      "~/Content/site.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new ScriptBundle("~/bundles/first").Include(
                        "~/bower_components/modernizr/modernizr.js",
                        "~/bower_components/moment/min/moment-with-locales.js",
                        "~/bower_components/lodash/dist/lodash.js",
                        "~/bower_components/respond/src/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/support").Include(
                        //"~/bower_components/requirejs/require.js",
                        "~/bower_components/jquery/dist/jquery.js",
                        //"~/bower_components/jasmine/lib/jasmine.js",

                        "~/bower_components/bootstrap/dist/js/bootstrap.js",

                        "~/bower_components/angular/angular.js",
                        "~/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
                        "~/bower_components/angular-route/angular-route.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/app/app.js"));

            //Uncomment the following line if you want to test bundling and minification while in debug mode
            //BundleTable.EnableOptimizations = true;

        }
    }
}
