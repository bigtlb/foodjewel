﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Code
{
    public static class ControllerExtensions
    {

        public static IHttpActionResult GetErrorResult(this ApiController obj, IdentityResult result)
        {
            if (result == null)
            {
                return new InternalServerErrorResult(obj);
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        obj.ModelState.AddModelError("", error);
                    }
                }

                if (obj.ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return new BadRequestResult(obj);
                }

                return new InvalidModelStateResult(obj.ModelState, obj);
            }

            return null;
        }

        public class ForbiddenActionResult : IHttpActionResult
        {
            private readonly ApiController controller;
            private readonly string reason;
            public ForbiddenActionResult(ApiController controller, string reason)
            {
                this.controller = controller;
                this.reason = reason;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                response.ReasonPhrase = reason;
                return Task.FromResult(response);
            }
        }

        public static ForbiddenActionResult ForbiddenResult(this ApiController obj, string reason)
        {
            return new ForbiddenActionResult(obj, reason);
        }
    }
}