﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Code
{
    public class DateTimeWrapper
    {
        public static implicit operator DateTimeOffset(DateTimeWrapper p)
        {
            return DateTime.SpecifyKind(p._dt, DateTimeKind.Utc);
        }

        public static implicit operator DateTimeWrapper(DateTimeOffset dto)
        {
            return new DateTimeWrapper(dto.DateTime);
        }

        public static implicit operator DateTime(DateTimeWrapper dtr)
        {
            return dtr._dt;
        }

        public static implicit operator DateTimeWrapper(DateTime dt)
        {
            return new DateTimeWrapper(dt);
        }

        protected DateTimeWrapper(DateTime dt)
        {
            _dt = dt;
        }

        private readonly DateTime _dt;
    }
}