﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using FoodJewel.Models.Domain;
using System.Web.OData;
using FoodJewel;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Controllers.OData
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using FoodJewel.Models.Domain;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<JournalEntry>("JournalEntries");
    builder.EntitySet<JournalUser>("JournalUsers"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    [Authorize]
    public class JournalEntriesController : ODataController
    {
        private ApplicationUserManager userManager;
        private ApplicationDbContext dbContext;

        public JournalEntriesController(ApplicationUserManager userManager, ApplicationDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }

        // GET: odata/JournalEntries
        [EnableQuery(PageSize = 100)]
        public IQueryable<JournalEntry> GetJournalEntries()
        {
            return dbContext.JournalEntries;
        }

        // GET: odata/JournalEntries(5)
        [EnableQuery]
        public SingleResult<JournalEntry> GetJournalEntry([FromODataUri] int key)
        {
            return SingleResult.Create(dbContext.JournalEntries.Where(journalEntry => journalEntry.Id == key));
        }

        // PUT: odata/JournalEntries(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, JournalEntry journalEntry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != journalEntry.Id)
            {
                return BadRequest();
            }

            dbContext.Entry(journalEntry).State = EntityState.Modified;

            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JournalEntryExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(journalEntry);
        }

        // POST: odata/JournalEntries
        public async Task<IHttpActionResult> Post(JournalEntry journalEntry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            dbContext.JournalEntries.Add(journalEntry);
            await dbContext.SaveChangesAsync();

            return Created(journalEntry);
        }

        // PATCH: odata/JournalEntries(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<JournalEntry> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            JournalEntry journalEntry = await dbContext.JournalEntries.FindAsync(key);
            if (journalEntry == null)
            {
                return NotFound();
            }

            patch.Patch(journalEntry);

            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JournalEntryExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(journalEntry);
        }

        // DELETE: odata/JournalEntries(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            JournalEntry journalEntry = await dbContext.JournalEntries.FindAsync(key);
            if (journalEntry == null)
            {
                return NotFound();
            }

            dbContext.JournalEntries.Remove(journalEntry);
            await dbContext.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/JournalEntries(5)/User
        [EnableQuery]
        public SingleResult<JournalUser> GetUser([FromODataUri] int key)
        {
            return SingleResult.Create(dbContext.JournalEntries.Where(m => m.Id == key).Select(m => m.User));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private bool JournalEntryExists(int key)
        {
            return dbContext.JournalEntries.Count(e => e.Id == key) > 0;
        }
    }
}
