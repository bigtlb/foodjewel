﻿using FoodJewel.Models.Domain;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.OData;
using System.Web.OData.Routing;
using FoodJewel.Code;
using System.Web.Http.Results;
using System.Web.OData.Query;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Controllers.OData
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using FoodJewel.Models.Domain;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<JournalUser>("JournalUsers");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    [ODataRoutePrefix("JournalUsers")]
    [Authorize]
    public class JournalUsersController : ODataController
    {
        private ApplicationUserManager userManager;
        private ApplicationDbContext dbContext;
        public JournalUsersController(ApplicationUserManager userManager, ApplicationDbContext dbContext)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
        }

        // GET: odata/JournalUsers
        [EnableQuery(PageSize=100)]
        [ODataRoute]
        public IQueryable<JournalUser> GetJournalUsers()
        {
            var qry = dbContext.JournalUsers.AsQueryable();


            //If you aren't an administrator, you can only see yourself
            if (!User.IsInRole("Administrators"))
            {
                var userId = User.Identity.GetUserId();
                qry = qry.Where(r => r.IdentityUserId == userId);
            }

            return qry;
            
        }

        // GET: odata/JournalUsers(5)
        [EnableQuery]
        [ODataRoute]
        public SingleResult<JournalUser> GetJournalUser([FromODataUri] int key)
        {
            var qry = dbContext.JournalUsers.Where(journalUser => journalUser.Id == key);

            //If you aren't an administrator, you can only see yourself
            if (!User.IsInRole("Administrators"))
            {
                var userId = User.Identity.GetUserId();
                qry = qry.Where(r => r.IdentityUserId == userId);
            }
            return SingleResult.Create(qry);
        }

        // PUT: odata/JournalUsers(5)
        [ODataRoute]
        public async Task<IHttpActionResult> Put([FromODataUri] int key, JournalUser journalUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //If not an administrator, you better be trying to add yourself
            if (key != journalUser.Id
                || (!User.IsInRole("Administrators") &&
                !string.Equals(journalUser.UserName, User.Identity.GetUserName(), System.StringComparison.CurrentCultureIgnoreCase)) &&
                !string.Equals(journalUser.IdentityUserId, User.Identity.GetUserId(), System.StringComparison.InvariantCultureIgnoreCase))
            {
                return this.ForbiddenResult("Only administrators may update other JournalUsers.");
            }

            if (string.IsNullOrWhiteSpace(journalUser.IdentityUserId) && string.IsNullOrWhiteSpace(journalUser.UserName))
            {
                var state = new ModelStateDictionary();
                state.AddModelError("", "IdentityUserId or UserName must be provided.");
                return this.BadRequest(state);
            }

            //Normalize IdentityUserId, Username, and FullName with AspNetUser
            ApplicationUser user = null;

            if (string.IsNullOrWhiteSpace(journalUser.IdentityUserId))
            {
                //If no IdentityUserId, find by UserName
                user = await userManager.FindByNameAsync(journalUser.UserName);
                journalUser.IdentityUserId = user.Id;
            }
            else
            {
                //Else, find by IdentityUserId and update the UserName on the login, if it is different
                user = await userManager.FindByIdAsync(journalUser.IdentityUserId);
                if (!string.Equals(user.UserName, journalUser.UserName))
                {
                    user.UserName = journalUser.UserName;
                }
            }



            if (string.IsNullOrWhiteSpace(journalUser.FullName))
                journalUser.FullName = user.FullName;
            else
                user.FullName = journalUser.FullName;

            dbContext.Entry(journalUser).State = EntityState.Modified;

            try
            {
                using (DbContextTransaction txn = dbContext.Database.BeginTransaction())
                {
                    //First try to update the userManager
                    var result = await userManager.UpdateAsync(user);
                    if (!result.Succeeded)
                        this.GetErrorResult(result);

                    //If that succeeds, then update the JournalUser
                    await dbContext.SaveChangesAsync();
                    txn.Commit();
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JournalUserExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(journalUser);
        }

        // POST: odata/JournalUsers
        [ODataRoute]
        public async Task<IHttpActionResult> Post(JournalUser journalUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await userManager.FindByNameAsync(journalUser.UserName);

            if (!User.IsInRole("Administrators") && !string.Equals(journalUser.UserName, User.Identity.GetUserName(), System.StringComparison.CurrentCultureIgnoreCase))
            {
                return this.ForbiddenResult("Only administrators may create other JournalUsers.");
            }

            if (user != null)
            {
                //Normalize IdentityUserId, Username, and FullName with AspNetUser
                journalUser.IdentityUserId = user.Id;
                if (string.IsNullOrWhiteSpace(journalUser.UserName))
                    journalUser.UserName = user.UserName;
                else
                    user.UserName = journalUser.UserName;
                if (string.IsNullOrWhiteSpace(journalUser.FullName))
                    journalUser.FullName = user.FullName;
                else
                    user.FullName = journalUser.FullName;
            }

            using (DbContextTransaction txn = dbContext.Database.BeginTransaction())
            {
                if (user != null)
                {
                var result = await userManager.UpdateAsync(user);
                if (!result.Succeeded)
                    return this.GetErrorResult(result);
                }
                dbContext.JournalUsers.Add(journalUser);
                await dbContext.SaveChangesAsync();

                txn.Commit();
            }

            return Created(journalUser);
        }

        // PATCH: odata/JournalUsers(5)
        [AcceptVerbs("PATCH", "MERGE")]
        [ODataRoute]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<JournalUser> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            JournalUser journalUser = await dbContext.JournalUsers.FindAsync(key);
            if (journalUser == null)
            {
                return NotFound();
            }

            //Can't patch the IdentityUser relationship
            if (patch.GetChangedPropertyNames().Contains("IdentityUserId"))
            {
                return BadRequest("Cannot update IdentityUserId");
            }

            var user = await userManager.FindByIdAsync(journalUser.IdentityUserId);
            if (user == null
                || (!User.IsInRole("Administrators") && journalUser.IdentityUserId != User.Identity.GetUserId()))
            {
                return BadRequest("Cannot patch specified user.");
            }

            patch.Patch(journalUser);

            //Make journalUser changes first, then Normalize IdentityUser, UserName, FullName with AspNetUser
            journalUser.IdentityUserId = user.Id;
            if (string.IsNullOrWhiteSpace(journalUser.UserName))
                journalUser.UserName = user.UserName;
            else
                user.UserName = journalUser.UserName;
            if (string.IsNullOrWhiteSpace(journalUser.FullName))
                journalUser.FullName = user.FullName;
            else
                user.FullName = journalUser.FullName;

            try
            {
                using (DbContextTransaction txn = dbContext.Database.BeginTransaction())
                {
                    var result = await userManager.UpdateAsync(user);
                    if (!result.Succeeded) return this.GetErrorResult(result);

                    await dbContext.SaveChangesAsync();
                    txn.Commit();
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JournalUserExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(journalUser);
        }

        // DELETE: odata/JournalUsers(5)
        [ODataRoute]
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            JournalUser journalUser = await dbContext.JournalUsers.FindAsync(key);
            if (journalUser == null)
            {
                return NotFound();
            }

            if ((!User.IsInRole("Administrators") &&
                !string.Equals(journalUser.UserName, User.Identity.GetUserName(), System.StringComparison.CurrentCultureIgnoreCase)) &&
                !string.Equals(journalUser.IdentityUserId, User.Identity.GetUserId(), System.StringComparison.InvariantCultureIgnoreCase))
            {
                return this.ForbiddenResult("Only administrators may delete other JournalUsers.");
            }
            dbContext.JournalUsers.Remove(journalUser);
            await dbContext.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private bool JournalUserExists(int key)
        {
            return dbContext.JournalUsers.Count(e => e.Id == key) > 0;
        }
    }
}
