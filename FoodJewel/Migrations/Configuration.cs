using FoodJewel.Models.Domain;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity.Migrations;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Migrations
{


    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //------------Comment in this code if you need to debug the seed method------------

            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();

            context.Configuration.LazyLoadingEnabled = true;
            context.Clients.AddOrUpdate(r => r.Id,
                new OAuthClient { Id = "FoodJewel-WinPhone8.1", Secret = "ClientSecretPassword", Enabled = true },
                new OAuthClient { Id = "FoodJewel-Win8.1", Secret = "ClientSecretPassword", Enabled = true });

            var user = new ApplicationUser
            {
                Id = "29efcd88-52b2-4449-aa0b-2fe52dc495f0",
                Email = "admin@heliconsoftware.com",
                EmailConfirmed = false,
                PasswordHash = "AGGcuxCH5aicirKRZB2xJH3dfVDsdzsta7gDV6MnmLyMXKXB0R0PngL1SILYuw27qg==",  //password = P@ssw0rd                
                SecurityStamp = "179db40b-9f8c-4bf7-a075-cbd115ae2bb9",
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = false,
                AccessFailedCount = 0,
                UserName = "admin",
                FullName = "Administrator"
            };
            context.Users.AddOrUpdate(r => r.UserName, user);

            //Make Administrators role, if it doesn't already exist
            var role = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Administrators" };
            context.Roles.AddOrUpdate(r => r.Name, role);

            //Join the two records together
            var userrole = new IdentityUserRole { UserId = user.Id, RoleId = role.Id };
            user.Roles.Add(userrole);

            user = new ApplicationUser
            {
                Id = "57e8d6ab-b1e5-4f32-b326-c0ffe1c4f131",
                Email = "unittest@heliconsoftware.com",
                EmailConfirmed = false,
                PasswordHash = "ACnsVdBa8lYYXydy6WDk7SvCvUkV/MM5329V+KY2KLu91atsDN/DpVE8qDCe6yPOdg==",  //Password = Unittest1
                SecurityStamp = "ff85248d-f319-4c84-b14d-dfe8247ac9c2",
                PhoneNumberConfirmed = false,
                TwoFactorEnabled = false,
                LockoutEnabled = false,
                AccessFailedCount = 0,
                UserName = "unittest",
                FullName = "Unit Test User"
            };
            context.Users.AddOrUpdate(r => r.UserName, user);

            context.SaveChanges();
        }
    }
}
