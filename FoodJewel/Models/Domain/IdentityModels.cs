﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [StringLength(255)]
        public string FullName { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            await SynchronizeJournalUser(manager.Context.Get<ApplicationDbContext>());

            // Add custom user claims here
            return userIdentity;
        }

        public async Task SynchronizeJournalUser(ApplicationDbContext db)
        {
            //Validate that a JournalUser exists
            JournalUser user = await db.JournalUsers.SingleOrDefaultAsync(r => r.IdentityUserId == this.Id);
            if (user == null)
            {
                //Make JournalUser record
                user = new JournalUser
                {
                    UserName = this.UserName,
                    FullName = this.FullName,
                    IdentityUserId = this.Id
                };
                db.JournalUsers.Add(user);
            }
            else
            {
                //Resynch the username and FullName, in case the were changed
                user.UserName = this.UserName;
                user.FullName = this.FullName;
            }
            await db.SaveChangesAsync();
        }
    }

    public class RefreshToken
    {
        [Key]
        public Guid Id { get; set; }
        public string ClientId { get; set; }
        public virtual OAuthClient Client { get; set; }
        public DateTime Expiration { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public string Ticket { get; set; }
    }

    public class OAuthClient
    {
        [Key]
        public string Id { get; set; }
        //public string SecretHash { get; set; }
        public string Secret { get; set; }
        public bool Enabled { get; set; }
    }

    public class ApplicationUserStore : UserStore<ApplicationUser>
    {
        public ApplicationUserStore(ApplicationDbContext dbcontext) : base(dbcontext) { }

        public override Task CreateAsync(ApplicationUser user)
        {
            return base.CreateAsync(user);
        }

    }

}