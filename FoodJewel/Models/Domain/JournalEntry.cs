﻿using FoodJewel.Code;
using System;
using System.ComponentModel.DataAnnotations.Schema;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{
    public class JournalEntry
    {
        private DateTimeWrapper dtw = default(DateTime);
        public int Id { get; set; }
        [Index]
        public DateTime Date
        {
            get { return dtw; }
            set { dtw = value; }
        }
        [NotMapped]
        public DateTimeOffset DateOffset
        {
            get { return dtw; }
            set { dtw = value; }
        }
        [Index]
        public int UserId { get; set; }
        public JournalUser User { get; set; }
    }
}