﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{
    //Storage is always either in (in/cm or lb/kg)
    public enum MeasureTypeEnum
    {
        lb,     //Pounds
        kg,     //Kilograms
        st,     //Stones, Lbs  (1 Stone = 14 lb)  Stored in lbs
        ft,     //Feet & Inches (stored in inches)
        m,      //Meters    (stored in cm)
        cm,     //Centimeteres  
    }

    [ComplexType]
    public class Measurement
    {
        public decimal Value { get; set; }
        public MeasureTypeEnum Format { get; set; }
    }
}
