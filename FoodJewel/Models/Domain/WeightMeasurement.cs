﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Models.Domain
{

    public class WeightMeasurement : Measurement, IFormattable
    {
        public void SetLb(decimal? lb, decimal? oz)
        {
            Format = MeasureTypeEnum.lb;
            Value = (lb.HasValue ? lb.Value : 0m)
                + (oz.HasValue ? oz.Value / 16 : 0m);
        }

        public void SetStone(decimal? stone, decimal? lb)
        {
            Format = MeasureTypeEnum.st;
            Value = (lb.HasValue ? lb.Value : 0m)
                + (stone.HasValue ? stone.Value * 14 : 0m);
        }

        public void SetKg(decimal? kg)
        {
            Format = MeasureTypeEnum.kg;
            Value = kg ?? 0m;
        }

        public decimal GetLb()
        {
            return Convert(false, Value);
        }

        public decimal GetStone(out decimal lb)
        {
            var val = Convert(false, Value);
            lb = val % 14;
            return Math.Floor(val / 14);
        }

        public decimal GetKg()
        {
            return Convert(true, Value);
        }

        public override string ToString()
        {
            return ToString("G", CultureInfo.CurrentCulture);
        }

        public string ToString(string format)
        {
            return ToString(format, CultureInfo.CurrentCulture);
        }

        public string ToString(IFormatProvider formatProvider)
        {
            return ToString("G", formatProvider);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrWhiteSpace(format)) format = "G";
            if (formatProvider == null) formatProvider = CultureInfo.CurrentCulture;
            var culture = (formatProvider as CultureInfo) ?? CultureInfo.CurrentCulture;
            var region = new RegionInfo(culture.LCID);

            switch (format.ToUpperInvariant())
            {
                case "G":
                    return Convert(region.IsMetric, Value).ToString("G", formatProvider) + (region.IsMetric ? " kg" : " lb");
                case "X":
                    switch (Format)
                    {
                        case MeasureTypeEnum.lb:
                            return Value.ToString("G") + " lb";
                        case MeasureTypeEnum.kg:
                            return Value.ToString("G") + " kg";
                        case MeasureTypeEnum.st:
                            return (Value / 14).ToString("G") + " st";
                        default:
                            return "Invalid format";
                    }
                case "X1":
                    var major = Math.Floor(Value);
                    var minor = Value - Math.Floor(Value);
                    switch (Format)
                    {
                        case MeasureTypeEnum.lb:
                            return string.Format("{0:F0} lb{1}", major, minor == 0 ? "" : string.Format(" {0:F0} oz", minor * 16));
                        case MeasureTypeEnum.kg:
                            return Value.ToString("G") + " kg";
                        case MeasureTypeEnum.st:
                            major = Math.Floor(Value/14);
                            minor = Value % 14;
                            return string.Format("{0:F0} st{1}", major, minor==0?"":string.Format(" {0:G} lb", minor));
                        default:
                            return "Invalid format";
                    }
                default:
                    return "Invalid format specifier";
            }
        }

        private decimal Convert(bool convertToMetric, decimal val)
        {
            //Do we need to convert?
            if (convertToMetric != (Format == MeasureTypeEnum.kg))
            {
                //lb to kg conversion factor = 0.45359237
                var factor = convertToMetric ?
                    0.45359237m :       //lb to kg
                    1m / 0.45359237m;   //kg to lb 
                val = val * factor;
            }
            return val;
        }
    }
}
