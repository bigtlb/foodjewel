﻿using FoodJewel.Models.Domain;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;
using System.Linq;

//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

namespace FoodJewel.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var dbContext = context.OwinContext.Get<ApplicationDbContext>();

            var guid = Guid.NewGuid();
            var clientId = context.OwinContext.Get<string>("as:client_id");
            if (string.Equals(clientId, "FoodJewel-Web", StringComparison.Ordinal)) return;  //Don't provide refresh tokens to the web
            var userId = (await userManager.FindByNameAsync(context.Ticket.Properties.Dictionary["userName"])).Id;

            var serialTicket = context.SerializeTicket();
            // maybe only create a handle the first time, then re-use
            dbContext.RefreshTokens.Add(
                new RefreshToken()
                {
                    Id = guid,
                    ClientId = clientId,
                    Expiration = DateTime.Now.AddMonths(1),
                    UserId = userId,
                    Ticket = serialTicket
                });
            //Remove all other expired refresh tokens.
            dbContext.RefreshTokens.RemoveRange(dbContext.RefreshTokens.Where(r => r.UserId == userId && r.Expiration < DateTime.Now));

            //Only allow the last 10 refresh tokens to work.
            //Older refresh tokens could be an attack, or a security vulnerability.
            //Possibilty of using the application on more than 10 devices simultaneously is remote.
            dbContext.RefreshTokens.RemoveRange(dbContext.RefreshTokens.Where(r => r.UserId == userId).OrderByDescending(r => r.Expiration).Skip(10));

            await dbContext.SaveChangesAsync();

            context.SetToken(guid.ToString());
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var dbContext = context.OwinContext.Get<ApplicationDbContext>(); string serialTicket;
            var refreshId = Guid.Parse(context.Token);

            var refreshToken = dbContext.RefreshTokens.SingleOrDefault(r => r.Id == refreshId);

            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.Ticket);
                dbContext.RefreshTokens.Remove(refreshToken);

                //Remove all other expired refresh tokens
                dbContext.RefreshTokens.RemoveRange(dbContext.RefreshTokens.Where(r => r.UserId == refreshToken.UserId && r.Expiration < DateTime.Now));
                await dbContext.SaveChangesAsync();
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            CreateAsync(context).Wait();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            ReceiveAsync(context).Wait();
        }
    }
}