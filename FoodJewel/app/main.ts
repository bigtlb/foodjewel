﻿//===========================================================================
//      FoodJewel
//      Copyright 2014 - Thomas L Baker
//     
//      Licensed under the Apache License, Version 2.0 (the "License");
//      you may not use this file except in compliance with the License.
//      You may obtain a copy of the License at
//     
//         http://www.apache.org/licenses/LICENSE-2.0
//     
//      Unless required by applicable law or agreed to in writing, software
//      distributed under the License is distributed on an "AS IS" BASIS,
//      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//      See the License for the specific language governing permissions and
//      limitations under the License.
//===========================================================================

class Main {
    app: ng.IModule;

    constructor() {


        // create the module and name it foodJewelApp
        this.app = angular.module('foodJewelApp', ['ngRoute'])
            .config(routeConfig)
            .controller('mainController', mainController)
            .controller('aboutController', aboutController)
            .controller('logonController', logonController)
            .controller('indexController', indexController);
        angular.bootstrap(document, ['foodJewelApp']);
    }


}

class routeConfig {
    static $inject = ['$routeProvider', '$locationProvider'];
    constructor($routeProvider, $locationProvider) {

        $routeProvider
        // route for the home page - if we used this, the path would be href= "#/' which is awkward
        //So we let the otherwise operator handle everything
        //.when('/', {
        //    templateUrl: './app/pages/index.html',
        //    controller: 'indexController'
        //})

        // route for the about page
            .when('/about', {
                templateUrl: './app/pages/about.html',
                controller: 'aboutController'
            })

        // route for the contact page
            .when('/logon', {
                templateUrl: './app/pages/logon.html',
                controller: 'logonController'
            })
            .otherwise({
                templateUrl: './app/pages/index.html',
                controller: 'indexController'
            });

    }
}

new Main();
